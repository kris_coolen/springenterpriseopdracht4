package be.kriscoolen.cursusenterprisespring.opdracht4;

import be.kriscoolen.cursusenterprisespring.opdracht4.domain.Beer;
import be.kriscoolen.cursusenterprisespring.opdracht4.domain.BeerOrder;
import be.kriscoolen.cursusenterprisespring.opdracht4.domain.BeerOrderItem;
import be.kriscoolen.cursusenterprisespring.opdracht4.exceptions.InvalidNumberException;
import be.kriscoolen.cursusenterprisespring.opdracht4.repository.BeerOrderRepository;
import be.kriscoolen.cursusenterprisespring.opdracht4.repository.BeerRepository;
import be.kriscoolen.cursusenterprisespring.opdracht4.service.BeerService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class BeerApp {
    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(BeerApp.class,args);

        /*BeerRepository beerRepository = ctx.getBean("beerRepository",BeerRepository.class);

        System.out.println("Beer with id=20:");
        Beer beer = beerRepository.getBeerById(20);
        System.out.println(beer);

        System.out.println("Beers with alcohol 1.0:");
        List<Beer> beers = beerRepository.getBeerByAlcohol(1.0f);
        System.out.println(beers);

        Beer updateBeer = beerRepository.getBeerById(4);
        System.out.println("Beer with id 4:");
        System.out.println(updateBeer);
        System.out.println("We update the stock to 1000 and price to 1.50");
        updateBeer.setStock(1000);
        updateBeer.setPrice(1.50f);
        beerRepository.updateBeer(updateBeer);
        System.out.println("Check in DB if beer is updated:");
        System.out.println(beerRepository.getBeerById(4));

        System.out.println("Now we will create a beerOrder existing of twee BeerOrderItems:");
        Beer beer1 = beerRepository.getBeerById(80);
        BeerOrderItem item1 = new BeerOrderItem();
        item1.setBeer(beer1);
        item1.setNumber(10);
        Beer beer2 = beerRepository.getBeerById(81);
        BeerOrderItem item2 = new BeerOrderItem();
        item2.setBeer(beer2);
        item2.setNumber(8);
        List<BeerOrderItem> beerOrderItemList = new ArrayList<>();
        beerOrderItemList.add(item1);
        beerOrderItemList.add(item2);
        BeerOrder beerOrder = new BeerOrder();
        beerOrder.setName("Bestelling Omar");
        beerOrder.setItems(beerOrderItemList);
        BeerOrderRepository beerOrderRepository = ctx.getBean("beerOrderRepository",BeerOrderRepository.class);
        beerOrderRepository.saveOrder(beerOrder);*/

        BeerService beerService = ctx.getBean("beerService",BeerService.class);
        try{
            beerService.orderBeer("bier-bestelling",5,-5);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        try{
            beerService.orderBeer("bier-bestelling",50000,10);
        }catch(Exception e){
        System.out.println(e.getMessage());
        }
        try{
            beerService.orderBeer("bier-bestelling",5,100000);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        try{
            beerService.orderBeer("bier-bestelling",5,25);
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
}
