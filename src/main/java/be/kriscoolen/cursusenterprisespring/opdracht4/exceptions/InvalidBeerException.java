package be.kriscoolen.cursusenterprisespring.opdracht4.exceptions;

public class InvalidBeerException extends Exception {

    public InvalidBeerException(String message){
        super(message);
    }
}
