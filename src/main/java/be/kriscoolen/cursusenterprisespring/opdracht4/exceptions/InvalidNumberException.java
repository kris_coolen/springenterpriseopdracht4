package be.kriscoolen.cursusenterprisespring.opdracht4.exceptions;

public class InvalidNumberException extends Exception {

    public InvalidNumberException(String message){
        super(message);
    }
}
