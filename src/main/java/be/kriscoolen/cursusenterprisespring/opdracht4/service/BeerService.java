package be.kriscoolen.cursusenterprisespring.opdracht4.service;

import be.kriscoolen.cursusenterprisespring.opdracht4.exceptions.InvalidBeerException;
import be.kriscoolen.cursusenterprisespring.opdracht4.exceptions.InvalidNumberException;

public interface BeerService {
    public int orderBeer(String name, int beerId, int number) throws InvalidBeerException, InvalidNumberException;
    public int orderBeers(String name, int[][] order) throws InvalidBeerException, InvalidNumberException;
}
