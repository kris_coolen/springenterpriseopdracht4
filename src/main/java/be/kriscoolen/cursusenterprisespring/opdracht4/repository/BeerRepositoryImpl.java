package be.kriscoolen.cursusenterprisespring.opdracht4.repository;

import be.kriscoolen.cursusenterprisespring.opdracht4.domain.Beer;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository("beerRepository")
public class BeerRepositoryImpl implements BeerRepository {

    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public Beer getBeerById(int id) {
        return entityManager.find(Beer.class,id);
    }

    @Override
    @Transactional
    public List<Beer> getBeerByAlcohol(float alcohol) {
        TypedQuery<Beer> query = entityManager.createQuery(
                "select b from Beer as b where b.alcohol=:PARAM_ALCOHOL",Beer.class);
        query.setParameter("PARAM_ALCOHOL",alcohol);
        return query.getResultList();
    }

    @Override
    @Transactional
    //@Transactional(propagation = Propagation.MANDATORY) //throws exception
    //@Transactional(propagation = Propagation.REQUIRES_NEW) //werkt niet als je bier wil bestellen
    //@Transactional(propagation = Propagation.SUPPORTS) //throws exception
    //@Transactional(propagation = Propagation.NEVER) //throws exception
    public void updateBeer(Beer beer) {
        Beer beerDB = getBeerById(beer.getId());
        if(beerDB!=null) {
            beerDB = beer;
            entityManager.merge(beerDB);
        }
    }
}
