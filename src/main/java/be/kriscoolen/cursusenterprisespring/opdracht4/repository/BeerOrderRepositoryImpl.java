package be.kriscoolen.cursusenterprisespring.opdracht4.repository;

import be.kriscoolen.cursusenterprisespring.opdracht4.domain.BeerOrder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository("beerOrderRepository")
public class BeerOrderRepositoryImpl implements BeerOrderRepository {

    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public int saveOrder(BeerOrder order) {
        entityManager.persist(order);
        return order.getId();
    }

    @Override
    @Transactional
    public BeerOrder getBeerOrderById(int id) {
        return entityManager.find(BeerOrder.class,id);
    }
}
