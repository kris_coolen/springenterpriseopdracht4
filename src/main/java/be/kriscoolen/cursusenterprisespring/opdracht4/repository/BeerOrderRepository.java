package be.kriscoolen.cursusenterprisespring.opdracht4.repository;

import be.kriscoolen.cursusenterprisespring.opdracht4.domain.BeerOrder;

public interface BeerOrderRepository {
    public int saveOrder(BeerOrder order);
    public BeerOrder getBeerOrderById(int id);
}
