package be.kriscoolen.cursusenterprisespring.opdracht4.repository;

import be.kriscoolen.cursusenterprisespring.opdracht4.domain.Beer;

import java.util.List;

public interface BeerRepository {
    public Beer getBeerById(int id);
    public List<Beer> getBeerByAlcohol(float alcohol);
    public void updateBeer(Beer beer);
}
